package com.pikulyk.cars;

public class Sedan extends Car {

  public Sedan(String brand, String model, String typeOfEngine,
      int horsepower, int seats) {
    super(brand, model, typeOfEngine, horsepower, seats);
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName() + ": "
        + super.toString();
  }


}
