package com.pikulyk.manager;

import com.pikulyk.cars.*;
import java.util.*;
import java.util.stream.Collectors;

public class Filter {

  Creator creator = new Creator();
  List<Car> cars = new ArrayList<Car>();

  public final List<Car> filterByMaxSpeed(final int maxSpeed) {
    if (cars == null) {
      cars = creator.getCars().stream()
          .filter(car -> car.getHorsepower() >= maxSpeed)
          .sorted(Comparator.comparingInt(Car::getHorsepower))
          .collect(Collectors.toList());
    } else {
      cars = cars.stream()
          .filter(car -> car.getHorsepower() >= maxSpeed)
          .sorted(Comparator.comparingInt(Car::getHorsepower))
          .collect(Collectors.toList());
    }
    return cars;
  }
}
