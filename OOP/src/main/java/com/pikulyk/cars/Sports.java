package com.pikulyk.cars;

public class Sports extends Car {

  private boolean sportMode = true;

  public Sports(String brand, String model, String typeOfEngine, int horsepower,
      int seats, boolean sportMode) {
    super(brand, model, typeOfEngine, horsepower, seats);
    this.sportMode = sportMode;
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName() + ": "
        + super.toString() + "SportMode: " + sportMode;
  }

  public boolean hasSportMode() {
    return sportMode;
  }
}
