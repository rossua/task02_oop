package com.pikulyk.manager;

import com.pikulyk.cars.*;
import java.util.*;


public class Creator {

  private List<Car> cars;

  public Creator() {
    createCars();
  }

  public List<Car> getCars() {
    return cars;
  }

  private void createCars() {
    cars = new ArrayList<Car>();
    cars.add(new Truck("Ford", "Super Duty F-450", "Diesel",
    450, 5, true));
    cars.add(new Truck("Chevrolet", "Colorado", "Diesel",
    200, 5, true));
    cars.add(new Truck("Mercedes-Benz", "814", "Petrol",
        150, 3, false));
    cars.add(new Truck("GMC", "Sierra2500HD", "Petrol",
    360,3 ,true));
    cars.add(new Sedan("Mercedes-Benz", "C43", "Petrol",
    390, 5));
    cars.add(new Sedan("Mercedes-Benz", "Maybach S 500" , "Petrol",
        455, 5));
    cars.add(new Sedan("Audi", "A6", "Diesel", 340, 5));
    cars.add(new Sedan("Audi", "A8", "Diesel", 350, 5));
    cars.add(new Sedan("Porsche", "Panamera 4", "Diesel",
        350, 5));
    cars.add(new Sports("Tesla", "Model S P100D", "Electric",
        762, 5, false));
    cars.add(new Sports("Audi", "R8 Coupe", "Diesel",
        610, 2, true));
    cars.add(new Sports("Lamborghini", "Aventador", "Petrol",
        740, 2, true));
    cars.add(new Sports("Lamborghini", "Centenario", "Petrol",
        770, 2, true));
    cars.add(new Sports("Porsche", "911 Carrera 4S", "Petrol",
        450, 2, true));
  }
}
