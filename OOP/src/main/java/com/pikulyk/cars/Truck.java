package com.pikulyk.cars;

public class Truck extends Car {

  private boolean trailer;

  public Truck(String brand, String model, String typeOfEngine, int horsepower,
      int seats, boolean trailer) {
    super(brand, model, typeOfEngine, horsepower, seats);
    this.trailer = trailer;
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName() + ": "
        + super.toString() + ", Trailer: " + trailer;
  }

  public final boolean hasTrailer() {
    return trailer;
  }
}
