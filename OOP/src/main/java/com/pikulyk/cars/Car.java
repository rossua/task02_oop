package com.pikulyk.cars;

public abstract class Car {

  private String brand;
  private String model;
  private String typeOfEngine;
  private int horsepower;
  private int seats;

  public Car(String brand, String model, String typeOfEngine,
      int horsepower, int seats) {
    this.brand = brand;
    this.model = model;
    this.typeOfEngine = typeOfEngine;
    this.horsepower = horsepower;
    this.seats = seats;
  }

  public String getBrand() {
    return brand;
  }

  public String getModel() {
    return model;
  }

  public String getTypeOfEngine() {
    return typeOfEngine;
  }

  public int getHorsepower() {
    return horsepower;
  }

  public int getSeats() {
    return seats;
  }

  @Override
  public String toString() {
    return "Brand: " + brand
        + " ,Model: " + model
        + " ,Type of Engine: " + typeOfEngine
        + " ,Horsepower: " + horsepower
        + " ,Seats: " + seats;
  }
}
